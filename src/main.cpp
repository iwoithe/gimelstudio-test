#include <QApplication>
#include <QDebug>
#include <QGuiApplication>
#include <QMetaType>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QVariantList>

#include "framework/actions/dispatcher.h"
#include "framework/actions/dispatchermanager.h"

#include "framework/objects/baseobject.h"
#include "framework/objects/rectangle.h"
#include "framework/objects/viewport.h"

#include "framework/selection/selectionmodel.h"


using namespace GimelStudio::Framework::Actions;


int main(int argc, char** argv) {
    Dispatcher *dis = new Dispatcher;
    setDispatcher(dis);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<GimelStudio::Framework::Objects::BaseObject>("GimelStudio.Objects", 0, 7, "GSBaseObject");
    qmlRegisterType<GimelStudio::Framework::Objects::Rectangle>("GimelStudio.Objects", 0, 7, "GSRectangle");
    qmlRegisterType<GimelStudio::Framework::Objects::Viewport>("GimelStudio.Objects", 0, 7, "GSViewport");

    qmlRegisterType<GimelStudio::Framework::Selection::SelectionModel>("GimelStudio.Selection", 0, 7, "SelectionModel");

    const QUrl url("qrc:/ui/main.qml");
    engine.load(url);

    return app.exec();
}

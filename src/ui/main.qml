import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import Qt.labs.platform 1.1
import QtQuick.Layouts 1.15

import GimelStudio.Objects 0.7
import GimelStudio.Selection 0.7

Window {
    id: root
    title: qsTr("Gimel Studio")
    width: 800
    height: 600
    visible: true

    function createRects(numRects) {
        for (var i = 0; i < numRects; i++) {
            const newRect = Qt.createQmlObject(`
                import GimelStudio.Objects 0.7

                GSRectangle {
                    name: "Rect ${i}"
                    width: 100
                    height: 100
                    x: 0
                    y: 0
                }
            `, viewport, "DynamicRectangle")
        }
    }

    Label {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 20
        anchors.bottomMargin: 20

        z: 10

        text: selectionModel.count
    }

    GSViewport {
        id: viewport
        name: "Viewport"
        anchors.fill: parent

        Component.onCompleted: root.createRects(1000);
    }

    SelectionModel {
        id: selectionModel
    }
}
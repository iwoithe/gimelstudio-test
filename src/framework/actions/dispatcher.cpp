#include <QDebug>
#include "dispatcher.h"

using namespace GimelStudio::Framework::Actions;


void Dispatcher::addFunction(const QString &name, VoidFunction func) {
    Action *newAction = new Action();
    newAction->name = name;
    newAction->func = func;

    m_actions.push_back(newAction);
}


void Dispatcher::callFunction(const QString &name, QVariantList args) {
    findAction(name)->func(args);
}


Action* Dispatcher::findAction(const QString &name) {
    for (int i = 0; i < m_actions.size(); i++) {
        if (m_actions.at(i)->name == name) {
            return m_actions.at(i);
        }
    }

    return 0;
}

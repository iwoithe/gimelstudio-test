#ifndef DISPATCHERMANAGER_H
#define DISPATCHERMANAGER_H

#include "dispatcher.h"

using namespace GimelStudio::Framework::Actions;

static Dispatcher* m_dispatcher;
Dispatcher* dispatcher();
void setDispatcher(Dispatcher* dis);

#endif

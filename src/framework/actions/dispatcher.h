#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <functional>
#include <QDebug>
#include <QList>
#include <QString>
#include <QVariantList>
#include <vector>


namespace GimelStudio::Framework::Actions
{
    class Dispatcher;

    typedef std::function<bool(QVariantList)> BoolFunction;
    typedef std::function<void(QVariantList)> VoidFunction;

    struct Action {
        QString name;
        VoidFunction func;
    };
} // namespace GimelStudio::Framework::Actions


using namespace GimelStudio::Framework::Actions;


class GimelStudio::Framework::Actions::Dispatcher
{
private:
    std::vector<Action*> m_actions = {};

public:
    Dispatcher() {};
    ~Dispatcher() {};

    void addFunction(const QString &name, VoidFunction func);
    void callFunction(const QString &name, QVariantList args);
    Action* findAction(const QString &name);
};


#endif

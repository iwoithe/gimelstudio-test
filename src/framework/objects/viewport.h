#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <QBrush>
#include <QColor>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QQuickItem>
#include <QQuickPaintedItem>

#include "baseobject.h"


namespace GimelStudio::Framework::Objects
{
    class Viewport;
} // namespace GimelStudio::Framework::Objects


class GimelStudio::Framework::Objects::Viewport : public GimelStudio::Framework::Objects::BaseObject
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit Viewport(QQuickItem* parent = nullptr);
    void paint(QPainter *painter) override;

    virtual void mousePressEvent(QMouseEvent *event);
};


#endif

#ifndef BASEOBJECT_H
#define BASEOBJECT_H

#include <QPainter>
#include <QQuickItem>
#include <QQuickPaintedItem>
#include <QString>


namespace GimelStudio::Framework::Objects
{
    class BaseObject;
} // namespace GimelStudio::Framework::Objects


class GimelStudio::Framework::Objects::BaseObject : public QQuickPaintedItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)
signals:
    void nameChanged();
    void selectedChanged();
private:
    QString m_name;
    bool m_selected = false;
public:
    explicit BaseObject(QQuickItem* parent = nullptr);

    QString name();
    void setName(QString val);

    bool selected();
    void setSelected(bool val);
};


#endif

#include <QColor>
#include <QPen>
#include <QVariantList>

#include "rectangle.h"
#include "framework/actions/dispatchermanager.h"


using namespace GimelStudio::Framework::Objects;


Rectangle::Rectangle(QQuickItem* parent)
    : GimelStudio::Framework::Objects::BaseObject(parent)
{
    setFlag(ItemHasContents, true);
    // TODO: When updating to Qt 6.3+, set ItemObservesViewport flag 
    setAcceptedMouseButtons(Qt::AllButtons);
    setAntialiasing(true);
}

void Rectangle::paint(QPainter *painter) {
    painter->setBrush(m_backgroundColor);
    painter->drawRect(0, 0, width(), height());
}


QColor Rectangle::backgroundColor() {
    return m_backgroundColor;
}


void Rectangle::setBackgroundColor(QColor val) {
    if (m_backgroundColor == val) {
        return;
    }

    m_backgroundColor = val;
    emit backgroundColorChanged(m_backgroundColor);
}


void Rectangle::mousePressEvent(QMouseEvent *event) {
    m_offset = event->pos();

    if (!selected()) {
        QVariantList args;
        QVariant thisVariant = QVariant::fromValue(this);
        args << thisVariant;
        dispatcher()->callFunction("add-objects-to-selection", args);
        setSelected(true);
    }
}


void Rectangle::mouseMoveEvent(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton) {
        // setPosition(mapToScene(event->pos() - m_offset));
        setPosition(mapToItem(static_cast<QQuickItem *>(parent()), event->pos() - m_offset));
    }
}

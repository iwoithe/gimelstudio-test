#include <QColor>

#include "baseobject.h"


using namespace GimelStudio::Framework::Objects;

BaseObject::BaseObject(QQuickItem* parent)
    : QQuickPaintedItem(parent)
{
}

QString BaseObject::name() {
    return m_name;
}

void BaseObject::setName(QString val) {
    if (m_name == val) {
        return;
    }

    m_name = val;
    emit nameChanged();
}


bool BaseObject::selected() {
    return m_selected;
}


void BaseObject::setSelected(bool val) {
    if (m_selected == val) {
        return;
    }
    
    m_selected = val;
    emit selectedChanged();
}

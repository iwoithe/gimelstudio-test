#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <QBrush>
#include <QColor>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QQuickItem>
#include <QQuickPaintedItem>

#include "baseobject.h"


namespace GimelStudio::Framework::Objects
{
    class Rectangle;
} // namespace GimelStudio::Framework::Objects


class GimelStudio::Framework::Objects::Rectangle : public GimelStudio::Framework::Objects::BaseObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)

private:
    QColor m_backgroundColor = QColor("#C8C8C8");
    QPen m_backgroundPen;

    QPoint m_offset;

signals:
    void backgroundColorChanged(QColor val);

public:
    explicit Rectangle(QQuickItem* parent = nullptr);
    void paint(QPainter *painter) override;

    QColor backgroundColor();
    void setBackgroundColor(QColor val);

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
};


#endif

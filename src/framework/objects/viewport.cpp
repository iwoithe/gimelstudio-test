#include "viewport.h"

#include "framework/actions/dispatchermanager.h"

using namespace GimelStudio::Framework::Objects;


Viewport::Viewport(QQuickItem* parent)
    : GimelStudio::Framework::Objects::BaseObject(parent)
{
    setFlag(ItemClipsChildrenToShape, true);
    setFlag(ItemHasContents, true);
    // setFlag(ItemAcceptsDrops, true);
    // TODO: When updating to Qt 6.3+, set ItemIsViewport flag 
    setAcceptedMouseButtons(Qt::AllButtons);
}


void Viewport::paint(QPainter *painter) {
    painter->setBrush(QColor("#525252"));
    painter->drawRect(0, 0, width(), height());
}


void Viewport::mousePressEvent(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton) {
        QVariantList args;
        dispatcher()->callFunction("clear-selection", args);
    }
}

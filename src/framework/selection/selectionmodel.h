#ifndef SELECTIONMODEL_H
#define SELECTIONMODEL_H

#include <QAbstractListModel>
#include <QByteArray>
#include <QHash>
#include <QList>
#include <QModelIndex>
#include <QObject>
#include <Qt>
#include <QVariant>

#include "framework/objects/baseobject.h"


using namespace GimelStudio::Framework::Objects;


namespace GimelStudio::Framework::Selection
{
    class SelectionModel;
} // namespace GimelStudio::Framework::Selection


class GimelStudio::Framework::Selection::SelectionModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int count READ count NOTIFY countChanged)
signals:
    void countChanged(int val);
private:
    QList<QVariant> m_data;
    QHash<int, QByteArray> m_roleNames;
public:
    explicit SelectionModel(QObject *parent = 0);

    enum RoleNames {
        ObjectRole = Qt::UserRole
    };

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    int count();

    void addObjectsToSelection(QVariantList args);
    void clearSelection(QVariantList args);

    void append(QVariant value);
    void insert(int index, QVariant value);
};


#endif

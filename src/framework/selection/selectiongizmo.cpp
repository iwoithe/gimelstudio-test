#include "selectiongizmo.h"

#include "framework/actions/dispatchermanager.h"


using namespace GimelStudio::Framework::Selection;


SelectionGizmo::SelectionGizmo(QQuickItem* parent)
    : GimelStudio::Framework::Objects::BaseObject(parent)
{
    setFlag(ItemHasContents, true);
    // TODO: When updating to Qt 6.3+, set ItemObservesViewport flag 
    setAcceptedMouseButtons(Qt::AllButtons);
    setAntialiasing(true);
}

void SelectionGizmo::paint(QPainter *painter) {
    painter->setPen(Qt::red);
    painter->drawRect(0, 0, width(), height());
}


void SelectionGizmo::mousePressEvent(QMouseEvent *event) {
    m_offset = event->pos();
}


void SelectionGizmo::mouseMoveEvent(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton) {
        // setPosition(mapToScene(event->pos() - m_offset));
        setPosition(mapToItem(static_cast<QQuickItem *>(parent()), event->pos() - m_offset));
    }
}

#ifndef SELECTIONGIZMO_H
#define SELECTIONGIZMO_H

#include "framework/objects/baseobject.h"


namespace GimelStudio::Framework::Selection
{
    class SelectionGizmo;
} // namespace GimelStudio::Framework::Selection


class GimelStudio::Framework::Selection::SelectionGizmo : public GimelStudio::Framework::Objects::BaseObject
{
    Q_OBJECT
    QML_ELEMENT

private:
    QPoint m_offset;

public:
    explicit SelectionGizmo(QQuickItem* parent = nullptr);
    void paint(QPainter *painter) override;

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
};


#endif

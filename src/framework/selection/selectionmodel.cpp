#include <functional>

#include "selectionmodel.h"

#include "framework/objects/rectangle.h"
#include "framework/actions/dispatchermanager.h"


using namespace GimelStudio::Framework::Selection;


SelectionModel::SelectionModel(QObject *parent) : QAbstractListModel(parent)
{
    dispatcher()->addFunction("add-objects-to-selection", [this](QVariantList args) {
        this->addObjectsToSelection(args);
    });

    dispatcher()->addFunction("clear-selection", [this](QVariantList args) {
        this->clearSelection(args);
    });
}


int SelectionModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return m_data.count();
}


QVariant SelectionModel::data(const QModelIndex &index, int role) const {
    int row = index.row();

    if (row < 0 || row >= m_data.count()) {
        return QVariant();
    }

    switch (role) {
        case ObjectRole:
            return m_data[row];
        default:
            return QVariant();
    }
}


QHash<int, QByteArray> SelectionModel::roleNames() const {
    return m_roleNames;
}


int SelectionModel::count() {
    return m_data.count();
}


void SelectionModel::addObjectsToSelection(QVariantList args) {
    for (int i = 0; i < args.count(); i++) {
        append(args[i]);
    }
}


void SelectionModel::clearSelection(QVariantList args) {
    emit beginRemoveRows(QModelIndex(), 0, m_data.count() - 1);
    for (int i = 0; i < m_data.count(); i++) {
        m_data.removeAt(i);
    }
    emit endRemoveRows();
    emit countChanged(m_data.count());
}


void SelectionModel::append(QVariant value) {
    insert(m_data.count(), value);
}


void SelectionModel::insert(int index, QVariant value) {
    if (index < 0 || index > m_data.count()) {
        return;
    }

    emit beginInsertRows(QModelIndex(), index, index);
    m_data.insert(index, value);
    emit endInsertRows();
    emit countChanged(m_data.count());
}
